import React, { Component } from 'react';
import { Container, Dropdown, Header, Icon, Menu } from 'semantic-ui-react';
import { history } from '../utils/history';

class LayoutHeader extends Component {
  logout = () => {
    sessionStorage.clear();
    history.push('/')
  }

  render() {
    return (
      <Container className="header">
        <Menu fixed='top'>
          <Container>
            <Menu.Item>
              <Header as='h2'>
                <Icon name='bug'/>
                <Header.Content>Code Test</Header.Content>
              </Header>
            </Menu.Item>
            <Menu.Item position='right'>
              <Dropdown simple item text='Options'>
                <Dropdown.Menu>
                  <Dropdown.Item onClick={this.logout}><Icon name='log out'/>Logout</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Menu.Item>
          </Container>
        </Menu>
      </Container>
    );
  }
}

export default LayoutHeader;
