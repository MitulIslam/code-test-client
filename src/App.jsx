import React, { Component } from 'react';
import { Provider } from "react-redux";
import { Router, Route, Switch } from "react-router-dom";
import { Container } from 'semantic-ui-react';

import PrivateRoute from './components/PrivateRoute';
import NotFound from './pages/NotFound';
import Login from './pages/Login';
import Signup from './pages/Signup';
import DashBoard from './pages/DashBoard';
import { history } from './utils/history';
import store from './redux/store';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Container>
          <Router history={history}>
            <Switch>
              <Route exact path="/" component={Login}/>
              <Route exact path="/signup" component={Signup}/>
              <PrivateRoute path="/dashboard" component={DashBoard}/>
              <Route component={NotFound}/>
            </Switch>
          </Router>
        </Container>
      </Provider>
    );
  }
}

export default App;
