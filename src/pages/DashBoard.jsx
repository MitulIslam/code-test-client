import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Container, Divider, Grid, List, Message } from 'semantic-ui-react';

import Header from '../components/Header';
import { loadData } from '../redux/api/content';
import { resetData } from '../redux/actions/data';

class DashBoard extends Component {
  state = {
    onInit: false,
    running : false,
    content: ''
  }

  componentDidUpdate() {
    if(this.state.onInit && this.state.running) {
      if(!this.props.loading && this.props.data.data.url) {
        this.props.loadData(this.props.data.data.url);
      }
      if(!this.props.loading && this.props.data.data.content) {
        this.setState({
          onInit: false,
          running : false,
          content: this.props.data.data.content,
        });
      }
    }
  }

  pause = () => {
    this.setState((state) => {
      return { running: !state.running }
    });
  }

  initiate = () => {
    this.props.resetData();
    this.setState((state) => {
      if (!state.running) {
        return { onInit: true, running: true, content: '' }
      }
    }, () => {
      this.props.loadData('http://0.0.0.0:3030/api/index');
    });
  }

  render() {
    const { onInit, running, content } = this.state; 
    const { list } = this.props.data;
    return (
      <Container>
        <Header />
        <Grid verticalAlign='middle' centered style={{ height: '100vh' }} columns={2}>
          <Grid.Row>
            <Grid.Column style={{ maxWidth: 450 }} verticalAlign='middle'>
              <Button.Group>
                <Button size='big' color='green' toggle disabled={onInit} onClick={this.initiate}>Initiate</Button>
                <Button size='big' color='red' toggle disabled={!onInit} active={!running} onClick={this.pause}>{running ? 'Pause' : 'Resume'}</Button>
              </Button.Group>
              <Divider/>
              <Message>
                Request count : {list.length <250 && <span style={{color: 'green'}}>{list.length}</span>}
                {list.length >=250 && list.length < 500 && <span style={{color: 'darkorange'}}>{list.length}</span>}
                {list.length >= 500 && <span style={{color: 'red'}}>{list.length}</span>}
              </Message>
              {
                content && <Message>Content : ${content}</Message>
              }
            </Grid.Column>
            <Grid.Column>
              <List style={{ maxHeight: '80vh', overflowY: 'scroll', marginTop: '5em'}}>
                {
                  list.map((item, index) => (
                    <List.Item key={index}>{JSON.stringify(item)}</List.Item>
                  ))
                }
              </List>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  data: state.data,
  loading: state.data.loading,
});

const mapDispatchToProps = dispatch => ({
  loadData: (url) => dispatch(loadData(url)),
  resetData: () => dispatch(resetData()),
});

export default connect(mapStateToProps, mapDispatchToProps)(DashBoard);