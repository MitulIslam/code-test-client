import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Container, Grid, Header, Icon, Form } from 'semantic-ui-react';

import { signup } from '../redux/api/auth';
import { isAuthenticated } from '../utils/auth';
import { history } from '../utils/history';

class Signup extends Component {
  state = {
    username: '',
    password: ''
  }

  componentDidMount() {
    isAuthenticated() && history.push('/dashboard');
  }

  onSubmit = event => {
    event.preventDefault();
    signup(this.state.username, this.state.password);
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  render() {
    return (
      <Container>
        <Grid verticalAlign='middle' centered style={{ height: '100vh' }}>
          <Grid.Row>
            <Grid.Column style={{ maxWidth: 450 }}>
              <Header as='h2'>
                <Icon name='bug'/>
                <Header.Content>Code Test</Header.Content>
              </Header>
              <Form>
                <Form.Field>
                  <label>Username</label>
                  <input placeholder='username' value={this.state.username} onChange={this.handleChange('username')} />
                </Form.Field>
                <Form.Field>
                  <label>Password</label>
                  <input placeholder='password' type='password' value={this.state.password} onChange={this.handleChange('password')}/>
                </Form.Field>
                <Button primary onClick={this.onSubmit}>Signup</Button>
                <Link to='/'><p style={{ margin: '1em 0' }}>Already have an account? Login here</p></Link>
              </Form>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}

export default Signup;