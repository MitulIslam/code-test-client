import React from 'react';

const NotFound = () => (
  <div>
    Sorry page does not exist
  </div>
);

export default NotFound;