const DATA = {
  GET_DATA_PENDING: 'GET_DATA_PENDING',
  GET_DATA_RESOLVE: 'GET_DATA_RESOLVE',
  GET_DATA_REJECTED: 'GET_DATA_REJECTED',
  DATA_RESET: 'DATA_RESET',
}

export default DATA;