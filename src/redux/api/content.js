import axios from 'axios';
import {
  loadDataPending,
  loadDataResolved,
  loadDataRejected,
} from '../actions/data';

export const loadData = url => async (dispatch) => {
  dispatch(loadDataPending());
  try {
    const result = await axios.get(url);
    dispatch(loadDataResolved(result.data));
  } catch (error) {
    dispatch(loadDataRejected(error));
  }
}