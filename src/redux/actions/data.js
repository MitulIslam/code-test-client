import DATA from '../types/data';

export const loadDataPending = () => ({
  type: DATA.GET_DATA_PENDING,
});

export const loadDataResolved = (payload) => ({
  type: DATA.GET_DATA_RESOLVE,
  payload: payload,
});

export const loadDataRejected = () => ({
  type: DATA.GET_DATA_REJECTED,
  payload: {},
});

export const resetData = () => ({
  type: DATA.DATA_RESET,
});