import DATA from '../types/data';

const defaultState = {
  loading: false,
  data: {},
  list: []
};

const dataReducer = (state = defaultState, action) => {
  switch (action.type) {
    case DATA.GET_DATA_PENDING:
      return { ...state, loading: true };
    case DATA.GET_DATA_RESOLVE:
      return { ...state, loading: false, data: action.payload, list: [...state.list, action.payload] };
    case DATA.GET_DATA_REJECTED:
      return { ...state, loading: false, data: action.payload };

    case DATA.DATA_RESET:
      return defaultState;

    default:
      return state;
  }
};

export default dataReducer;