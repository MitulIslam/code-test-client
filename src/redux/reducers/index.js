import { combineReducers } from "redux";

import dataReducer from './data';

const reducer = combineReducers({
  data: dataReducer,
});

export default reducer;